-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema acc16
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `acc16` ;

-- -----------------------------------------------------
-- Schema acc16
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `acc16` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `acc16` ;

-- -----------------------------------------------------
-- Table `acc16`.`publisher`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `acc16`.`publisher` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `acc16`.`publisher` (
  `pub_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pub_name` VARCHAR(45) NOT NULL,
  `pub_street` VARCHAR(30) NOT NULL,
  `pub_city` VARCHAR(30) NOT NULL,
  `pub_state` CHAR(2) NOT NULL,
  `pub_zip` INT UNSIGNED NOT NULL,
  `pub_phone` BIGINT UNSIGNED NOT NULL,
  `pub_email` VARCHAR(100) NOT NULL,
  `pub_url` VARCHAR(100) NOT NULL,
  `pub_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pub_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `acc16`.`book`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `acc16`.`book` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `acc16`.`book` (
  `bok_isbn` VARCHAR(13) NOT NULL,
  `pub_id` SMALLINT UNSIGNED NOT NULL,
  `bok_title` VARCHAR(100) NOT NULL,
  `bok_pub_date` DATE NOT NULL,
  `bok_num_pages` SMALLINT NOT NULL,
  `bok_cost` DECIMAL NOT NULL,
  `bok_price` DECIMAL NOT NULL,
  `bok_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`bok_isbn`),
  INDEX `fk_book_publisher1_idx` (`pub_id` ASC),
  CONSTRAINT `fk_book_publisher1`
    FOREIGN KEY (`pub_id`)
    REFERENCES `acc16`.`publisher` (`pub_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `acc16`.`member`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `acc16`.`member` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `acc16`.`member` (
  `mem_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `mem_fname` VARCHAR(15) NOT NULL,
  `mem_lname` VARCHAR(30) NOT NULL,
  `mem_street` VARCHAR(30) NOT NULL,
  `mem_city` VARCHAR(30) NOT NULL,
  `mem_state` CHAR(2) NOT NULL,
  `mem_zip` INT NOT NULL,
  `mem_phone` BIGINT NOT NULL,
  `mem_email` VARCHAR(100) NULL,
  `mem_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`mem_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `acc16`.`loaner`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `acc16`.`loaner` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `acc16`.`loaner` (
  `lon_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `mem_id` SMALLINT UNSIGNED NOT NULL,
  `bok_isbn` VARCHAR(13) NOT NULL,
  `lon_loan_date` DATE NOT NULL,
  `lon_due_date` DATE NOT NULL,
  `lon_return_date` DATE NULL,
  `lon_late_fee` DECIMAL(5,2) NULL,
  `lon_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`lon_id`),
  INDEX `fk_loaner_book_idx` (`bok_isbn` ASC),
  INDEX `fk_loaner_member1_idx` (`mem_id` ASC),
  CONSTRAINT `fk_loaner_book`
    FOREIGN KEY (`bok_isbn`)
    REFERENCES `acc16`.`book` (`bok_isbn`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_loaner_member1`
    FOREIGN KEY (`mem_id`)
    REFERENCES `acc16`.`member` (`mem_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `acc16`.`category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `acc16`.`category` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `acc16`.`category` (
  `cat_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cat_type` VARCHAR(45) NOT NULL,
  `cat_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cat_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `acc16`.`book_cat`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `acc16`.`book_cat` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `acc16`.`book_cat` (
  `bct_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `bok_isbn` VARCHAR(13) NOT NULL,
  `cat_id` SMALLINT UNSIGNED NOT NULL,
  `bct_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`bct_id`),
  INDEX `fk_book_cat_book1_idx` (`bok_isbn` ASC),
  INDEX `fk_book_cat_category1_idx` (`cat_id` ASC),
  CONSTRAINT `fk_book_cat_book1`
    FOREIGN KEY (`bok_isbn`)
    REFERENCES `acc16`.`book` (`bok_isbn`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_book_cat_category1`
    FOREIGN KEY (`cat_id`)
    REFERENCES `acc16`.`category` (`cat_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `acc16`.`author`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `acc16`.`author` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `acc16`.`author` (
  `aut_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `aut_fname` VARCHAR(15) NOT NULL,
  `aut_lname` VARCHAR(30) NOT NULL,
  `aut_street` VARCHAR(30) NOT NULL,
  `aut_city` VARCHAR(30) NOT NULL,
  `aut_state` CHAR(2) NOT NULL,
  `aut_zip` INT NOT NULL,
  `aut_phone` BIGINT NOT NULL,
  `aut_email` VARCHAR(100) NOT NULL,
  `aut_url` VARCHAR(100) NOT NULL,
  `aut_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`aut_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `acc16`.`attribution`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `acc16`.`attribution` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `acc16`.`attribution` (
  `att_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `aut_id` MEDIUMINT UNSIGNED NOT NULL,
  `bok_isbn` VARCHAR(13) NOT NULL,
  `att_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`att_id`),
  INDEX `fk_attribution_book1_idx` (`bok_isbn` ASC),
  INDEX `fk_attribution_author1_idx` (`aut_id` ASC),
  CONSTRAINT `fk_attribution_book1`
    FOREIGN KEY (`bok_isbn`)
    REFERENCES `acc16`.`book` (`bok_isbn`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_attribution_author1`
    FOREIGN KEY (`aut_id`)
    REFERENCES `acc16`.`author` (`aut_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `acc16`.`publisher`
-- -----------------------------------------------------
START TRANSACTION;
USE `acc16`;
INSERT INTO `acc16`.`publisher` (`pub_id`, `pub_name`, `pub_street`, `pub_city`, `pub_state`, `pub_zip`, `pub_phone`, `pub_email`, `pub_url`, `pub_notes`) VALUES (1, 'Harlan K. Harrell', '276-4436 Libero. St.', 'Lidingo', 'CA', 56624, 5709456174, 'eu@facilisiSedneque.co.uk', 'publishthis.com', 'purus,');
INSERT INTO `acc16`.`publisher` (`pub_id`, `pub_name`, `pub_street`, `pub_city`, `pub_state`, `pub_zip`, `pub_phone`, `pub_email`, `pub_url`, `pub_notes`) VALUES (2, 'Brooke Foley', '7054 Gravida Rd.', 'Kano', 'KN', 23261, 4895986097, 'nulla@nonummy.net', 'published.net', 'arcu imperdiet ullamcorper. Duis at');
INSERT INTO `acc16`.`publisher` (`pub_id`, `pub_name`, `pub_street`, `pub_city`, `pub_state`, `pub_zip`, `pub_phone`, `pub_email`, `pub_url`, `pub_notes`) VALUES (3, 'Amelia Meyer', ' 8218 Cras Ave', 'Maiduguri', 'NY', 16513, 8592085770, 'arcu.et.pede@ideratEtiam.ca', 'publishthis.com', 'scelerisque mollis. Phasellus libero mauris, aliquam eu,');
INSERT INTO `acc16`.`publisher` (`pub_id`, `pub_name`, `pub_street`, `pub_city`, `pub_state`, `pub_zip`, `pub_phone`, `pub_email`, `pub_url`, `pub_notes`) VALUES (4, 'Forrest Christensen', '461-2568 Odio. St', 'Laramie', 'WO', 53165, 3209547370, 'sit.amet.dapibus@leo.com', 'published.net', 'Donec sollicitudin adipiscing ligula. Aenean');
INSERT INTO `acc16`.`publisher` (`pub_id`, `pub_name`, `pub_street`, `pub_city`, `pub_state`, `pub_zip`, `pub_phone`, `pub_email`, `pub_url`, `pub_notes`) VALUES (5, 'Gretchen Hardin', '6200 Velit Avenue', 'Villenave-d\'Ornon', 'PA', 26516, 2962596418, 'eu.lacus.Quisque@semvitaealiquam.org', 'published.net', 'cursus purus. Nullam scelerisque neque sed sem');

COMMIT;


-- -----------------------------------------------------
-- Data for table `acc16`.`book`
-- -----------------------------------------------------
START TRANSACTION;
USE `acc16`;
INSERT INTO `acc16`.`book` (`bok_isbn`, `pub_id`, `bok_title`, `bok_pub_date`, `bok_num_pages`, `bok_cost`, `bok_price`, `bok_notes`) VALUES ('5425600000000', 1, 'dictum augue malesuada malesuada. Integer id', '2019-09-14', 328, 67.55 , 26.63, 'Nunc laoreet lectus quis massa. Mauris');
INSERT INTO `acc16`.`book` (`bok_isbn`, `pub_id`, `bok_title`, `bok_pub_date`, `bok_num_pages`, `bok_cost`, `bok_price`, `bok_notes`) VALUES ('5008840000000', 2, 'litora torquent per conubia nostra, per', '2020-05-06', 252, 49.06 , 42.54, 'hendrerit a, arcu. Sed et libero. Proin mi.');
INSERT INTO `acc16`.`book` (`bok_isbn`, `pub_id`, `bok_title`, `bok_pub_date`, `bok_num_pages`, `bok_cost`, `bok_price`, `bok_notes`) VALUES ('8936230000000', 3, 'Donec egestas. Aliquam nec enim. Nunc', '2020-08-12', 112, 86.35 , 12.39, 'eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis');
INSERT INTO `acc16`.`book` (`bok_isbn`, `pub_id`, `bok_title`, `bok_pub_date`, `bok_num_pages`, `bok_cost`, `bok_price`, `bok_notes`) VALUES ('4356610000000', 4, 'eu, odio. Phasellus at augue id', '2019-03-24', 460, 59.49 , 38.79, 'a odio semper cursus. Integer mollis. Integer tincidunt aliquam');
INSERT INTO `acc16`.`book` (`bok_isbn`, `pub_id`, `bok_title`, `bok_pub_date`, `bok_num_pages`, `bok_cost`, `bok_price`, `bok_notes`) VALUES ('1386840000000', 5, 'quam. Pellentesque habitant morbi tristique senectus', '2020-05-20', 234, 80.79 , 17.25, 'metus vitae');

COMMIT;


-- -----------------------------------------------------
-- Data for table `acc16`.`member`
-- -----------------------------------------------------
START TRANSACTION;
USE `acc16`;
INSERT INTO `acc16`.`member` (`mem_id`, `mem_fname`, `mem_lname`, `mem_street`, `mem_city`, `mem_state`, `mem_zip`, `mem_phone`, `mem_email`, `mem_notes`) VALUES (1, 'Gage', 'Fernandez', '390 Dictum Rd.', 'Provo', 'UT', 20593, 7893633148, 'ligula.elit.pretium@ut.org', 'libero. Morbi accumsan laoreet ipsum. Curabitur');
INSERT INTO `acc16`.`member` (`mem_id`, `mem_fname`, `mem_lname`, `mem_street`, `mem_city`, `mem_state`, `mem_zip`, `mem_phone`, `mem_email`, `mem_notes`) VALUES (2, 'Byron', 'Walters', '277 Ipsum Street', 'Kenosha', 'WI', 94382, 6664823425, 'enim.nisl@Sed.ca', 'dictum. Proin eget odio. Aliquam vulputate');
INSERT INTO `acc16`.`member` (`mem_id`, `mem_fname`, `mem_lname`, `mem_street`, `mem_city`, `mem_state`, `mem_zip`, `mem_phone`, `mem_email`, `mem_notes`) VALUES (3, 'Kelsie', 'Noble', '509 At Street.', 'Provo', 'UT', 52035, 5215289525, 'ornare.In@nequeMorbiquis.ca', 'tempus scelerisque, lorem ipsum');
INSERT INTO `acc16`.`member` (`mem_id`, `mem_fname`, `mem_lname`, `mem_street`, `mem_city`, `mem_state`, `mem_zip`, `mem_phone`, `mem_email`, `mem_notes`) VALUES (4, 'Jackson', 'Francis', '7695 Cras Av.', 'Athens', 'GA', 28927, 9836239689, 'mollis.Integer@scelerisque.com', 'Suspendisse ac metus vitae velit egestas lacinia. Sed congue, elit');
INSERT INTO `acc16`.`member` (`mem_id`, `mem_fname`, `mem_lname`, `mem_street`, `mem_city`, `mem_state`, `mem_zip`, `mem_phone`, `mem_email`, `mem_notes`) VALUES (5, 'Jolene', 'Ross', '407 Gravida Rd.', 'Meridian', 'ID', 92610, 5044939075, 'quis.lectus.Nullam@vitae.com', 'velit justo');

COMMIT;


-- -----------------------------------------------------
-- Data for table `acc16`.`loaner`
-- -----------------------------------------------------
START TRANSACTION;
USE `acc16`;
INSERT INTO `acc16`.`loaner` (`lon_id`, `mem_id`, `bok_isbn`, `lon_loan_date`, `lon_due_date`, `lon_return_date`, `lon_late_fee`, `lon_notes`) VALUES (4000, 1, '5425600000000', '2019-08-01', '2018-10-21', '2019-12-19', 25.50 , 'lobortis ultrices. Vivamus rhoncus. Donec est. Nunc ullamcorper, velit in');
INSERT INTO `acc16`.`loaner` (`lon_id`, `mem_id`, `bok_isbn`, `lon_loan_date`, `lon_due_date`, `lon_return_date`, `lon_late_fee`, `lon_notes`) VALUES (4001, 2, '5008840000000', '2019-05-27', '2019-11-18', '2018-10-25', 71.43 , 'Nulla eu neque pellentesque massa lobortis ultrices. Vivamus');
INSERT INTO `acc16`.`loaner` (`lon_id`, `mem_id`, `bok_isbn`, `lon_loan_date`, `lon_due_date`, `lon_return_date`, `lon_late_fee`, `lon_notes`) VALUES (4002, 3, '8936230000000', '2020-07-09', '2020-03-05', '2019-05-20', 41.63 , 'vel pede blandit congue.');
INSERT INTO `acc16`.`loaner` (`lon_id`, `mem_id`, `bok_isbn`, `lon_loan_date`, `lon_due_date`, `lon_return_date`, `lon_late_fee`, `lon_notes`) VALUES (4003, 4, '4356610000000', '2019-02-11', '2020-04-21', '2019-07-08', 87.59 , 'libero est, congue');
INSERT INTO `acc16`.`loaner` (`lon_id`, `mem_id`, `bok_isbn`, `lon_loan_date`, `lon_due_date`, `lon_return_date`, `lon_late_fee`, `lon_notes`) VALUES (4004, 5, '1386840000000', '2020-06-04', '2020-07-04', '2019-11-03', 81.08 , 'quis diam. Pellentesque');

COMMIT;


-- -----------------------------------------------------
-- Data for table `acc16`.`category`
-- -----------------------------------------------------
START TRANSACTION;
USE `acc16`;
INSERT INTO `acc16`.`category` (`cat_id`, `cat_type`, `cat_notes`) VALUES (1, 'horror', 'sem egestas blandit. Nam nulla magna,');
INSERT INTO `acc16`.`category` (`cat_id`, `cat_type`, `cat_notes`) VALUES (2, 'science fiction', 'et, rutrum eu, ultrices');
INSERT INTO `acc16`.`category` (`cat_id`, `cat_type`, `cat_notes`) VALUES (3, 'romance', 'scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed,');
INSERT INTO `acc16`.`category` (`cat_id`, `cat_type`, `cat_notes`) VALUES (4, 'comedy', 'ut mi. Duis risus odio, auctor vitae,');
INSERT INTO `acc16`.`category` (`cat_id`, `cat_type`, `cat_notes`) VALUES (5, 'thiller', 'purus. Duis elementum, dui quis accumsan convallis, ante lectus');

COMMIT;


-- -----------------------------------------------------
-- Data for table `acc16`.`book_cat`
-- -----------------------------------------------------
START TRANSACTION;
USE `acc16`;
INSERT INTO `acc16`.`book_cat` (`bct_id`, `bok_isbn`, `cat_id`, `bct_notes`) VALUES (10989, '5425600000000', 1, 'vitae, sodales at,');
INSERT INTO `acc16`.`book_cat` (`bct_id`, `bok_isbn`, `cat_id`, `bct_notes`) VALUES (11988, '5008840000000', 2, 'vitae diam. Proin dolor. Nulla semper tellus id nunc interdum');
INSERT INTO `acc16`.`book_cat` (`bct_id`, `bok_isbn`, `cat_id`, `bct_notes`) VALUES (12987, '8936230000000', 3, 'ultrices. Duis volutpat nunc sit');
INSERT INTO `acc16`.`book_cat` (`bct_id`, `bok_isbn`, `cat_id`, `bct_notes`) VALUES (13986, '4356610000000', 4, 'posuere cubilia Curae; Donec tincidunt. Donec');
INSERT INTO `acc16`.`book_cat` (`bct_id`, `bok_isbn`, `cat_id`, `bct_notes`) VALUES (14985, '1386840000000', 5, 'Nunc commodo auctor velit. Aliquam nisl. Nulla eu neque pellentesque');

COMMIT;


-- -----------------------------------------------------
-- Data for table `acc16`.`author`
-- -----------------------------------------------------
START TRANSACTION;
USE `acc16`;
INSERT INTO `acc16`.`author` (`aut_id`, `aut_fname`, `aut_lname`, `aut_street`, `aut_city`, `aut_state`, `aut_zip`, `aut_phone`, `aut_email`, `aut_url`, `aut_notes`) VALUES (3000, 'Melyssa', 'Walls', '925 Congue, Road', 'Mesa', 'AZ', 86496, 5899815468, 'vel.convallis.in@auctor.ca', 'author.com', 'amet massa. Quisque porttitor eros nec');
INSERT INTO `acc16`.`author` (`aut_id`, `aut_fname`, `aut_lname`, `aut_street`, `aut_city`, `aut_state`, `aut_zip`, `aut_phone`, `aut_email`, `aut_url`, `aut_notes`) VALUES (3001, 'Ima', 'Noel', '9056 Neque St.', 'Tulsa', 'OK', 84814, 6725914537, 'eleifend.nunc@gravidaAliquamtincidunt.co.uk', 'authone.net', 'nunc sed libero. Proin');
INSERT INTO `acc16`.`author` (`aut_id`, `aut_fname`, `aut_lname`, `aut_street`, `aut_city`, `aut_state`, `aut_zip`, `aut_phone`, `aut_email`, `aut_url`, `aut_notes`) VALUES (3002, 'Molly', 'Kelly', '221-8014 Vulputate, Rd.', 'Ketchikan', 'AK', 99932, 3429251888, 'felis@Donecatarcu.co.uk', 'authorme.co', 'at, egestas a, scelerisque');
INSERT INTO `acc16`.`author` (`aut_id`, `aut_fname`, `aut_lname`, `aut_street`, `aut_city`, `aut_state`, `aut_zip`, `aut_phone`, `aut_email`, `aut_url`, `aut_notes`) VALUES (3003, 'Cruz', 'Salazar', '8151 Scelerisque, Rd.', 'Sterling Heights', 'MI', 93440, 2199600006, 'cubilia@Fuscedolor.org', 'auth.org', 'euismod ac,');
INSERT INTO `acc16`.`author` (`aut_id`, `aut_fname`, `aut_lname`, `aut_street`, `aut_city`, `aut_state`, `aut_zip`, `aut_phone`, `aut_email`, `aut_url`, `aut_notes`) VALUES (3004, 'Davis', 'Olsen', '1736 Molestie St.', 'Athens', 'GA', 37817, 4739930181, 'nibh.sit@eratvolutpat.org', 'thorthesite.net', 'Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus vitae');

COMMIT;


-- -----------------------------------------------------
-- Data for table `acc16`.`attribution`
-- -----------------------------------------------------
START TRANSACTION;
USE `acc16`;
INSERT INTO `acc16`.`attribution` (`att_id`, `aut_id`, `bok_isbn`, `att_notes`) VALUES (1998, 3000, '5425600000000', 'Quisque porttitor eros nec');
INSERT INTO `acc16`.`attribution` (`att_id`, `aut_id`, `bok_isbn`, `att_notes`) VALUES (2997, 3001, '5008840000000', 'eget metus eu');
INSERT INTO `acc16`.`attribution` (`att_id`, `aut_id`, `bok_isbn`, `att_notes`) VALUES (3996, 3002, '8936230000000', 'risus. Morbi metus. Vivamus euismod');
INSERT INTO `acc16`.`attribution` (`att_id`, `aut_id`, `bok_isbn`, `att_notes`) VALUES (4995, 3003, '4356610000000', 'aliquet diam. Sed diam lorem, auctor quis, tristique');
INSERT INTO `acc16`.`attribution` (`att_id`, `aut_id`, `bok_isbn`, `att_notes`) VALUES (5994, 3004, '1386840000000', 'pretium neque. Morbi quis urna. Nunc');

COMMIT;

