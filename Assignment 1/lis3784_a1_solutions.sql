-- Part 1:

mysql> use acc16;
 -- select my database

mysql> show tables;
 -- show all data within the database

mysql> \. db/premiere.sql
 -- add tables in premiere.sql to active 
 
mysql> select * from orders;
 -- show data within orders table

mysql> select slsrep_number AS slsrep_num
    -> from sales_rep
    -> where slsrep_number='06';
 -- shows only data from sales_rep where slsrep_num is equal to '06'

mysql> select order_number, part_number, number_ordered, quoted_price
    -> from order_line
    -> order by quoted_price asc;
 -- selects only the order number, part number, number ordered and quoted price with the prices in ascending order. all listings are in ascending order so 'asc' is optional.

mysql> select * from part;
 -- show all from part

mysql> delete
    -> from part
    -> where part_number='cb03';
 -- deletes full section where the part number equals 'cb03' in the part table

mysql> select * from part;
 -- shows results of the delete

mysql> select * from sales_rep;
 -- shows all from sales_rep table

mysql> UPDATE sales_rep SET city='Tallahassee', state='FL', zip_code='32304' WHERE  slsrep_number='06';
 -- adds new data to replace the data where the slsrep_number equals '06'

mysql> select * from sales_rep;
 -- check updated table

mysql> select * from part;
 -- shows all data from the part table

mysql> insert into part
    -> (part_number, part_description, units_on_hand, item_class, warehouse_number , unit_price)
    -> values
    -> ('yyy','Widget1','5','SS',1,9.95),
    -> ('zzz','Widget2','10','TT',2,10.95);
 -- inserts two new sections in the table named 'Widget1' and 'Widget2'

mysql> select * from part;
 -- shows updated table



 -- Part 2

mysql> show tables;
 -- shows all tables in the database

mysql> select dlr_name, veh_type, veh_make
    -> from dealership, vehicle
    -> where dealership.dlr_id= vehicle.dlr_id;
 -- selects data only where the dealership.dlr_id matches vehicle.dlr_id;

mysql> select dlr_name, srp_fname,srp_lname, srp_tot_sales from dealership join slsrep on dealership.dlr_id = slsrep.dlr_id;
 -- selects needed data from dealership then matches those with the same dlr.id in dealerships with the dlr.id in slsrep

mysql> select d.dlr_id, dlr_name, srp_fname, srp_lname, srp_tot_sales
    -> from dealership as d
    -> join slsrep as s on d.dlr_id = s.dlr_id;
 -- assign dealerships as d and match the data in slsrep to the dealerships based on the dlr.id

mysql> select dlr_id, dlr_name, count(veh_type)
    -> from dealership
    -> join vehicle using (dlr_id)
    -> group by dlr_id;
 -- match dealership with vehicle using dlr.id and group by the same dlr.id
 
mysql> select dlr_name, sum(dhs_ytd_sales) as total_sales
    -> from dealership
    -> natural join dealership_history
    -> group by dlr_id;
 -- take the sum of the ytd_sales from dealership and match with dealership_history but group with dlr.id

