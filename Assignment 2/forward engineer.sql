-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema acc16
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `acc16` ;

-- -----------------------------------------------------
-- Schema acc16
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `acc16` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `acc16` ;

-- -----------------------------------------------------
-- Table `acc16`.`person`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `acc16`.`person` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `acc16`.`person` (
  `per_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `per_ssn` INT UNSIGNED NOT NULL,
  `per_fname` VARCHAR(15) NOT NULL,
  `per_lname` VARCHAR(30) NOT NULL,
  `per_gender` ENUM('m', 'f') NOT NULL,
  `per_dob` DATE NOT NULL,
  `per_street` VARCHAR(45) NOT NULL,
  `per_city` VARCHAR(45) NOT NULL,
  `per_state` CHAR(2) NOT NULL,
  `per_zip` INT UNSIGNED NOT NULL,
  `per_phone` BIGINT UNSIGNED NOT NULL,
  `per_email` VARCHAR(100) NOT NULL,
  `per_is_emp` ENUM('y', 'n') NOT NULL,
  `per_is_alm` ENUM('y', 'n') NOT NULL,
  `per_is_stu` ENUM('y', 'n') NOT NULL,
  `per_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  UNIQUE INDEX `per_ssn_UNIQUE` (`per_ssn` ASC))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `acc16`.`employee`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `acc16`.`employee` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `acc16`.`employee` (
  `per_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_title` VARCHAR(100) NOT NULL,
  `emp_salary` DECIMAL(8,2) NOT NULL,
  `emp_is_fac` ENUM('y', 'n') NOT NULL,
  `emp_is_stf` ENUM('y', 'n') NOT NULL,
  `emp_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  CONSTRAINT `fk_employee_Person`
    FOREIGN KEY (`per_id`)
    REFERENCES `acc16`.`person` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `acc16`.`alumnus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `acc16`.`alumnus` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `acc16`.`alumnus` (
  `per_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `alm_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  CONSTRAINT `fk_alumnus_Person1`
    FOREIGN KEY (`per_id`)
    REFERENCES `acc16`.`person` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `acc16`.`student`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `acc16`.`student` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `acc16`.`student` (
  `per_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `stu_major` VARCHAR(30) NOT NULL,
  `stu_is_ugd` ENUM('y', 'n') NOT NULL,
  `stu_is_grd` ENUM('y', 'n') NOT NULL,
  `stu_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  CONSTRAINT `fk_student_Person1`
    FOREIGN KEY (`per_id`)
    REFERENCES `acc16`.`person` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `acc16`.`faculty`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `acc16`.`faculty` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `acc16`.`faculty` (
  `per_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `fac_rank` VARCHAR(20) NOT NULL,
  `fac_start_date` DATE NOT NULL,
  `fac_end_date` DATE NULL,
  `fac_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  CONSTRAINT `fk_faculty_employee1`
    FOREIGN KEY (`per_id`)
    REFERENCES `acc16`.`employee` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `acc16`.`staff`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `acc16`.`staff` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `acc16`.`staff` (
  `per_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `stf_position` VARCHAR(30) NOT NULL,
  `stf_start_date` DATE NOT NULL,
  `stf_end_date` DATE NULL,
  `stf_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  CONSTRAINT `fk_staff_employee1`
    FOREIGN KEY (`per_id`)
    REFERENCES `acc16`.`employee` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `acc16`.`degree`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `acc16`.`degree` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `acc16`.`degree` (
  `deg_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `per_id` MEDIUMINT UNSIGNED NOT NULL,
  `deg_type` VARCHAR(30) NOT NULL,
  `deg_area` VARCHAR(30) NOT NULL,
  `deg_date` DATE NOT NULL,
  `deg_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`deg_id`),
  INDEX `fk_degree_alumnus1_idx` (`per_id` ASC),
  CONSTRAINT `fk_degree_alumnus1`
    FOREIGN KEY (`per_id`)
    REFERENCES `acc16`.`alumnus` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `acc16`.`undergrad`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `acc16`.`undergrad` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `acc16`.`undergrad` (
  `per_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ugd_test` ENUM('sat', 'act') NOT NULL,
  `ugd_score` SMALLINT NOT NULL,
  `ugd_standing` ENUM('freshman', 'sophomore', 'junior', 'senior') NOT NULL,
  `ugd_start_date` DATE NOT NULL,
  `ugd_end_date` DATE NULL,
  `ugd_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  CONSTRAINT `fk_undergrad_student1`
    FOREIGN KEY (`per_id`)
    REFERENCES `acc16`.`student` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `acc16`.`grad`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `acc16`.`grad` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `acc16`.`grad` (
  `per_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `grd_test` ENUM('gre', 'gmat', 'lsat', 'sat', 'act') NOT NULL,
  `grd_score` SMALLINT NOT NULL,
  `grd_start_date` DATE NOT NULL,
  `grd_end_date` DATE NULL,
  `grd_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  CONSTRAINT `fk_grad_student1`
    FOREIGN KEY (`per_id`)
    REFERENCES `acc16`.`student` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `acc16`.`person`
-- -----------------------------------------------------
START TRANSACTION;
USE `acc16`;
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (1, 719216657, 'Nissim', 'Doyle', 'f', '2019-09-15', '355-9729 Ut Rd.', 'Auburn', 'ME', 596190333, 1934744409, 'ridiculus.mus.Aenean@fermentumarcu.com', 'y', 'y', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (2, 746630890, 'Athena', 'Hunt', 'f', '2019-06-21', '9039 Tristique St.', 'San Francisco', 'CA', 456890230, 6911362639, 'Sed.pharetra@anunc.co.uk', 'y', 'n', 'n', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (3, 937552036, 'Allen', 'Franklin', 'm', '2019-04-04', 'P.O. Box 205, 1074 Felis Ave', 'Dover', 'DE', 392591351, 7001885033, 'dictum.eu@elitEtiamlaoreet.co.uk', 'n', 'n', 'n', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (4, 386006434, 'Maile', 'Romero', 'f', '2019-01-31', 'P.O. Box 521, 7642 In Road', 'Des Moines', 'IA', 233813120, 7911000360, 'eget.mollis.lectus@Nullam.net', 'n', 'n', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (5, 327707378, 'Hiram', 'Mccray', 'f', '2019-11-13', '367-4382 Nisi Av.', 'San Antonio', 'TX', 953168564, 3532366768, 'ornare@luctus.co.uk', 'n', 'y', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (6, 351136618, 'Iola', 'Berg', 'm', '2019-05-25', 'P.O. Box 987, 280 Nec, Ave', 'Kenosha', 'WI', 530271032, 9037446340, 'Phasellus@Morbisitamet.net', 'n', 'n', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (7, 545532213, 'Christopher', 'Haynes', 'f', '2018-12-10', '393-5079 Urna. Street', 'Tuscaloosa', 'AL', 820489644, 8400646102, 'Donec.egestas.Aliquam@veliteu.co.uk', 'y', 'n', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (8, 738713267, 'Abbot', 'Petersen', 'f', '2020-03-20', '554-9140 Metus Rd.', 'Mesa', 'AZ', 763741222, 3234335302, 'lorem.auctor.quis@nislelementumpurus.ca', 'y', 'n', 'n', 'Lorem ipsum');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (9, 923508492, 'Summer', 'Glenn', 'm', '2019-02-23', 'Ap #519-4446 Sapien, Rd.', 'Indianapolis', 'IN', 751298438, 1970885973, 'commodo@semper.net', 'y', 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (10, 117870225, 'Kiona', 'Newman', 'm', '2019-12-01', 'Ap #886-236 Nunc Avenue', 'Saint Paul', 'MN', 332573197, 222038562, 'laoreet.posuere@volutpatNullafacilisis.net', 'n', 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (11, 693677476, 'Hunter', 'Fisher', 'f', '2019-12-24', '824-9422 Hendrerit Street', 'Fort Smith', 'AR', 480089669, 6080483169, 'nulla.magna@dapibus.net', 'n', 'n', 'n', 'Lorem ipsum');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (12, 163953593, 'Vera', 'Erickson', 'f', '2020-05-26', 'P.O. Box 394, 7217 Euismod St.', 'Birmingham', 'AL', 705107343, 9133159338, 'Aliquam.gravida.mauris@varius.net', 'y', 'y', 'y', 'Lorem');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (13, 134048302, 'Wilma', 'Montgomery', 'f', '2019-10-09', '264-5741 Non, St.', 'Frankfort', 'KY', 681165326, 3288174079, 'In.tincidunt@leoCrasvehicula.ca', 'n', 'n', 'y', 'Lorem ipsum');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (14, 112812586, 'Grady', 'Beck', 'f', '2019-07-01', '905-6453 Arcu. Street', 'Baton Rouge', 'LA', 338738179, 8549362488, 'sodales.purus@tellus.org', 'y', 'y', 'y', 'Lorem ipsum dolor');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (15, 354339675, 'Quamar', 'Juarez', 'm', '2019-03-07', '7356 Vivamus Av.', 'Rock Springs', 'WY', 456798403, 6283955259, 'neque@morbi.net', 'n', 'y', 'y', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (16, 891747937, 'Constance', 'Perez', 'f', '2019-03-07', 'Ap #359-4804 Magna. St.', 'Fort Smith', 'AR', 984497684, 2656618786, 'et@euneque.net', 'y', 'n', 'n', 'Lorem ipsum');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (17, 968972559, 'Tanek', 'Cleveland', 'm', '2020-09-11', '607-388 Nulla St.', 'Gulfport', 'MS', 111547884, 3783131007, 'ut.pellentesque.eget@dis.edu', 'y', 'n', 'y', 'Lorem ipsum dolor');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (18, 291168198, 'Nina', 'Lester', 'f', '2019-01-01', '5506 Quisque Avenue', 'Indianapolis', 'IN', 872884521, 6968010728, 'egestas.Duis@turpisnecmauris.net', 'n', 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (19, 448015157, 'Ryder', 'Cline', 'm', '2018-12-03', 'Ap #920-6790 Neque Road', 'Baltimore', 'MD', 629647456, 4101453449, 'id@magnaetipsum.edu', 'y', 'y', 'y', 'Lorem ipsum');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (20, 205766209, 'Dillon', 'Henry', 'f', '2019-04-16', '137-3385 Lectus Street', 'Waterbury', 'CT', 621831150, 7371333811, 'justo.Praesent.luctus@sedpedeCum.net', 'y', 'n', 'n', 'Lorem');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (21, 467857199, 'Nina', 'Nixon', 'f', '2018-11-25', '6284 Dui Street', 'Waterbury', 'CT', 741042966, 9861675406, 'rutrum@tinciduntnequevitae.ca', 'y', 'y', 'n', 'Lorem ipsum dolor');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (22, 404209943, 'Tasha', 'Tyler', 'f', '2020-06-05', '4580 Mauris Road', 'Nampa', 'ID', 327834794, 1751779346, 'eget.metus.eu@dignissimmagnaa.org', 'y', 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (23, 358408272, 'Micah', 'Sykes', 'f', '2018-11-22', 'Ap #507-8230 In Avenue', 'Missoula', 'MT', 908374848, 1522568288, 'justo.nec.ante@eterosProin.com', 'y', 'n', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (24, 772716073, 'Mallory', 'Adkins', 'm', '2019-12-03', 'P.O. Box 694, 5723 Magna. Road', 'Houston', 'TX', 869542713, 226313178, 'Fusce.mollis@Integer.co.uk', 'n', 'y', 'n', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (25, 563362566, 'Leo', 'Mills', 'f', '2020-03-20', '576-3880 Morbi Avenue', 'Augusta', 'GA', 492105093, 6900416262, 'semper.Nam@aliquet.co.uk', 'y', 'y', 'n', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (26, 136194116, 'Chelsea', 'Petty', 'f', '2020-07-18', 'Ap #774-1763 Laoreet St.', 'Jacksonville', 'FL', 256143460, 7671805316, 'Ut@risusodio.edu', 'n', 'n', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (27, 186100641, 'Quamar', 'Barker', 'm', '2020-03-20', 'Ap #617-6014 Aenean Av.', 'Tacoma', 'WA', 278695625, 470413180, 'nascetur.ridiculus.mus@accumsaninterdumlibero.ca', 'n', 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (28, 192242218, 'Xyla', 'Gregory', 'm', '2020-01-13', 'Ap #458-5164 Amet Rd.', 'Tuscaloosa', 'AL', 258073236, 7135308458, 'ac@atfringilla.com', 'y', 'n', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (29, 559382090, 'Catherine', 'Roberts', 'f', '2019-10-11', '468-3576 Convallis Road', 'Paradise', 'NV', 204101582, 1072004409, 'vestibulum.Mauris@dictum.org', 'n', 'y', 'y', 'Lorem');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (30, 887855956, 'Eve', 'Barker', 'f', '2019-07-05', 'P.O. Box 869, 7528 Scelerisque St.', 'New Orleans', 'LA', 369825721, 5848947829, 'magna@egestasDuis.edu', 'n', 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (31, 967432083, 'Kevin', 'Riggs', 'f', '2020-02-21', 'Ap #338-512 Rhoncus. Road', 'Virginia Beach', 'VA', 799086900, 3508983344, 'ullamcorper.velit.in@Uttinciduntorci.org', 'n', 'n', 'n', 'Lorem');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (32, 957628233, 'Alec', 'Rutledge', 'm', '2019-02-11', '618-4794 Suspendisse Avenue', 'Louisville', 'KY', 616129732, 1691367998, 'pede.Nunc.sed@nonenimcommodo.com', 'y', 'y', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (33, 242008147, 'Inga', 'Moreno', 'f', '2019-03-01', '267-9658 Non Avenue', 'College', 'AK', 737795418, 5720385322, 'sollicitudin@Nullamscelerisque.ca', 'y', 'y', 'n', 'Lorem');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (34, 409334683, 'Dante', 'Schroeder', 'm', '2019-12-18', '240-5753 Pharetra St.', 'Biloxi', 'MS', 813452333, 1415026799, 'lacus.Mauris.non@nisinibhlacinia.net', 'y', 'n', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (35, 413111002, 'Solomon', 'England', 'f', '2019-04-14', '3964 Purus Av.', 'Juneau', 'AK', 884525751, 2187731406, 'ipsum.leo@molestie.edu', 'n', 'y', 'n', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (36, 653312624, 'MacKenzie', 'Vega', 'm', '2018-11-29', '651 Eu Avenue', 'Rock Springs', 'WY', 292961008, 5429876901, 'lacus@ultricessitamet.edu', 'y', 'y', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (37, 143775568, 'Ronan', 'Coleman', 'f', '2019-09-30', 'P.O. Box 526, 1184 Neque. Rd.', 'Newark', 'DE', 413633102, 9208252305, 'nulla.magna.malesuada@arcuVestibulum.co.uk', 'n', 'n', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (38, 626432359, 'Ava', 'Morse', 'f', '2018-11-27', 'P.O. Box 273, 9895 Ac, Av.', 'Jackson', 'MS', 538481063, 7547255859, 'congue@utquam.edu', 'n', 'y', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (39, 352089100, 'Jasmine', 'Humphrey', 'f', '2019-10-20', '225-5994 Nisl. Road', 'Springfield', 'MA', 596153678, 4202934639, 'odio.Aliquam.vulputate@semvitaealiquam.co.uk', 'n', 'y', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (40, 643160513, 'Camden', 'Hickman', 'm', '2020-01-17', '1845 Neque. Ave', 'Norman', 'OK', 235839484, 3622567094, 'velit@risus.com', 'y', 'n', 'n', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (41, 748538081, 'Zenia', 'Whitney', 'f', '2019-01-13', '258-2961 Praesent St.', 'Tallahassee', 'FL', 602220627, 6884374750, 'et@Classaptent.com', 'n', 'n', 'n', 'Lorem ipsum');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (42, 308656825, 'Darryl', 'Sweet', 'm', '2019-04-29', 'P.O. Box 929, 2273 Semper Av.', 'Carson City', 'NV', 438131642, 5338760284, 'Maecenas.malesuada@ametanteVivamus.edu', 'n', 'y', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (43, 833842394, 'Amber', 'Peterson', 'f', '2019-02-21', '771-7223 Dis Road', 'San Francisco', 'CA', 463455554, 988617868, 'Quisque.fringilla.euismod@Integerinmagna.net', 'n', 'n', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (44, 182113500, 'Oren', 'Cardenas', 'm', '2019-04-02', 'P.O. Box 298, 1449 At St.', 'Tacoma', 'WA', 732215217, 4277505096, 'est.Mauris@nibh.ca', 'y', 'n', 'y', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (45, 702686753, 'Kennan', 'Murray', 'f', '2020-08-01', '874 Donec St.', 'Saint Paul', 'MN', 503275442, 1803290350, 'enim.sit@venenatislacus.ca', 'y', 'y', 'n', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (46, 879960022, 'Chase', 'Webster', 'f', '2019-05-04', 'Ap #737-2263 Proin Av.', 'Montgomery', 'AL', 533834461, 4826136619, 'aliquet@diamPellentesque.org', 'y', 'y', 'y', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (47, 118412746, 'September', 'Macias', 'm', '2018-10-10', '704-8093 Dolor, St.', 'South Portland', 'ME', 357751143, 4781985965, 'Vestibulum@malesuada.ca', 'y', 'n', 'n', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (48, 405546590, 'Nash', 'Hobbs', 'm', '2020-09-03', 'P.O. Box 270, 5255 Lectus, Ave', 'Tacoma', 'WA', 242950211, 5341331709, 'Duis.dignissim@metus.edu', 'n', 'y', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (49, 125349741, 'Seth', 'Mcintyre', 'f', '2018-10-09', 'P.O. Box 157, 6565 Et Rd.', 'Montpelier', 'VT', 608209388, 935908897, 'vel@utdolor.edu', 'y', 'y', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (50, 460173723, 'Hedda', 'Knox', 'f', '2019-12-14', '387-9738 Scelerisque Rd.', 'Pittsburgh', 'PA', 454099405, 4627303584, 'eu@magnaa.edu', 'y', 'y', 'n', 'Lorem ipsum dolor');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (51, 327909873, 'Uriel', 'Mcfarland', 'f', '2019-04-25', '5148 Et, St.', 'San Jose', 'CA', 862843029, 2553769985, 'velit.Sed@ametornarelectus.com', 'y', 'n', 'n', 'Lorem ipsum');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (52, 497378031, 'Harding', 'Boyle', 'f', '2020-02-15', '814-7347 Adipiscing Ave', 'Oklahoma City', 'OK', 568091729, 6246752303, 'malesuada@odiovel.org', 'y', 'n', 'n', 'Lorem');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (53, 378812928, 'Seth', 'Sullivan', 'f', '2020-04-06', '586-5280 Facilisis. Street', 'Fort Worth', 'TX', 907877902, 8543300583, 'amet.metus@eumetusIn.ca', 'y', 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (54, 517818932, 'Geoffrey', 'Newman', 'f', '2019-01-19', 'Ap #328-7910 Nascetur St.', 'Honolulu', 'HI', 814800253, 1695979897, 'Proin@blandit.com', 'y', 'y', 'n', 'Lorem');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (55, 148450063, 'Timon', 'Copeland', 'f', '2020-05-21', 'P.O. Box 663, 694 Nunc. Road', 'South Bend', 'IN', 284483740, 9022947235, 'a.auctor.non@primisinfaucibus.org', 'n', 'y', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (56, 706843091, 'Francis', 'Oconnor', 'm', '2018-12-20', '4196 In Avenue', 'Auburn', 'ME', 369501414, 9822684696, 'at.auctor.ullamcorper@cursusnonegestas.org', 'y', 'n', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (57, 955746500, 'Catherine', 'Riley', 'f', '2019-03-05', '9335 Mollis. St.', 'Gaithersburg', 'MD', 878258183, 1145903849, 'a@liberoDonecconsectetuer.net', 'n', 'y', 'y', 'Lorem ipsum dolor');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (58, 218838415, 'Stacey', 'Schultz', 'm', '2020-08-11', 'Ap #719-6370 Quisque Av.', 'Des Moines', 'IA', 728584466, 5108255639, 'Cras.interdum.Nunc@noncursusnon.ca', 'y', 'y', 'y', 'Lorem ipsum');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (59, 204138646, 'Lucius', 'Lawson', 'm', '2020-08-07', 'Ap #168-2701 Dictum St.', 'Iowa City', 'IA', 329554036, 8643525799, 'auctor.odio.a@pedeCras.co.uk', 'y', 'y', 'n', 'Lorem ipsum');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (60, 933358713, 'Sarah', 'Coleman', 'f', '2019-10-04', '397-4047 Mauris, Av.', 'New Haven', 'CT', 852324667, 9779725148, 'luctus@Nullamsuscipit.org', 'y', 'y', 'n', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (61, 295420025, 'Leilani', 'Mcconnell', 'f', '2020-05-17', '827-1079 Est. Rd.', 'Clarksville', 'TN', 371747380, 2647302829, 'Donec@eutellus.com', 'n', 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (62, 739928743, 'Ezra', 'Kemp', 'm', '2019-07-08', 'Ap #765-7151 Magna Street', 'Eugene', 'OR', 257130157, 4731302419, 'tellus.imperdiet@Nunccommodo.net', 'y', 'y', 'n', 'Lorem ipsum');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (63, 690181642, 'Heidi', 'Evans', 'f', '2018-12-08', '5833 Ac Street', 'Laramie', 'WY', 618191345, 8084683492, 'lectus@ornare.org', 'n', 'n', 'y', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (64, 522909130, 'Yael', 'Fitzgerald', 'm', '2020-08-01', '5246 Scelerisque, Rd.', 'Chesapeake', 'VA', 999557397, 7154672178, 'ac.metus@eunibh.edu', 'y', 'y', 'y', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (65, 329075482, 'Mufutau', 'Hicks', 'm', '2019-10-25', '768-2148 Mollis. Road', 'Springdale', 'AR', 460738581, 6381103969, 'Phasellus.vitae@SedmolestieSed.ca', 'n', 'n', 'y', 'Lorem ipsum');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (66, 865148895, 'George', 'Ramos', 'f', '2019-10-22', 'Ap #669-9030 Feugiat Rd.', 'Sandy', 'UT', 400528128, 9466439185, 'tellus.Aenean.egestas@neque.com', 'y', 'n', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (67, 681440813, 'Regan', 'Roth', 'f', '2019-05-12', 'Ap #916-3944 At St.', 'San Antonio', 'TX', 382961092, 7708307097, 'arcu.imperdiet@malesuadamalesuadaInteger.com', 'n', 'n', 'n', 'Lorem ipsum');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (68, 491267010, 'Seth', 'Stevens', 'f', '2019-03-03', '552-4275 Habitant Road', 'Las Vegas', 'NV', 254847716, 6415118409, 'blandit.Nam@ligulaeu.co.uk', 'y', 'n', 'y', 'Lorem');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (69, 358559585, 'Malcolm', 'Silva', 'm', '2019-03-18', 'Ap #363-3501 A, Road', 'Kansas City', 'MO', 150673616, 5134651041, 'non.hendrerit@congueInscelerisque.ca', 'n', 'n', 'n', 'Lorem');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (70, 594533055, 'Colette', 'Allen', 'm', '2019-06-20', '6041 Parturient St.', 'Independence', 'MO', 135387714, 6410448410, 'quis@duiCras.ca', 'n', 'n', 'n', 'Lorem');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (71, 987361716, 'Delilah', 'Johns', 'm', '2019-11-11', 'P.O. Box 464, 5971 Sapien, St.', 'Nashville', 'TN', 202611627, 4435839344, 'malesuada.id.erat@lacus.com', 'y', 'n', 'n', 'Lorem ipsum');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (72, 943581777, 'Yuli', 'Robinson', 'm', '2019-04-21', '5935 Metus. St.', 'Bellevue', 'NE', 681129446, 2206305809, 'condimentum.eget@iaculis.org', 'y', 'n', 'n', 'Lorem');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (73, 196139015, 'Dexter', 'Olsen', 'm', '2018-10-21', 'Ap #692-6310 Mattis Rd.', 'Augusta', 'ME', 773493556, 2462526716, 'dolor@nec.co.uk', 'n', 'y', 'y', 'Lorem ipsum dolor');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (74, 205608735, 'Driscoll', 'Spencer', 'f', '2019-09-02', 'P.O. Box 136, 2274 Enim, St.', 'Kearney', 'NE', 619242603, 4145201098, 'pede.nonummy.ut@ornare.net', 'n', 'n', 'n', 'Lorem');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (75, 161537741, 'Silas', 'Kirkland', 'm', '2019-03-23', 'Ap #575-1614 Posuere St.', 'Kailua', 'HI', 917618675, 8358175517, 'at.auctor.ullamcorper@nec.ca', 'n', 'n', 'y', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (76, 822744035, 'Denton', 'Cochran', 'f', '2019-04-04', '710-5689 Semper Street', 'Louisville', 'KY', 357884760, 3399450078, 'nec@Cras.co.uk', 'y', 'n', 'y', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (77, 216683766, 'Xyla', 'Mcneil', 'm', '2018-10-11', '144-4576 Eget St.', 'Reno', 'NV', 734326653, 4176383694, 'vulputate.velit.eu@cubilia.org', 'y', 'y', 'y', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (78, 342783545, 'Buffy', 'Miranda', 'f', '2020-06-07', 'Ap #167-8545 Mi St.', 'Salem', 'OR', 625599532, 1921083626, 'congue.In@mattisvelitjusto.net', 'y', 'y', 'n', 'Lorem ipsum');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (79, 272839074, 'Jack', 'Lamb', 'm', '2019-12-31', 'P.O. Box 253, 7906 Consectetuer, Road', 'Tacoma', 'WA', 612301354, 3949849282, 'arcu.iaculis@inaliquetlobortis.edu', 'y', 'y', 'n', 'Lorem ipsum dolor');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (80, 127145029, 'Burton', 'Workman', 'f', '2019-04-09', '745-9102 A, Avenue', 'Indianapolis', 'IN', 155820831, 4530143306, 'turpis.vitae.purus@Maurismolestiepharetra.edu', 'n', 'n', 'n', 'Lorem');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (81, 884339699, 'Lester', 'Buchanan', 'f', '2019-04-26', 'Ap #338-8328 Neque. Road', 'Reading', 'PA', 565387270, 3451276019, 'fringilla.euismod@litora.co.uk', 'n', 'y', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (82, 120054458, 'Chadwick', 'Fulton', 'm', '2019-05-26', 'Ap #722-8161 Curae; Rd.', 'Pocatello', 'ID', 997852654, 6810473142, 'non.lorem@Quisqueac.net', 'y', 'y', 'y', 'Lorem ipsum');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (83, 982552183, 'Logan', 'Stephens', 'm', '2019-07-10', 'P.O. Box 372, 5153 Gravida. Ave', 'Nampa', 'ID', 496938037, 9604573969, 'et.magnis.dis@Fuscedolorquam.ca', 'y', 'y', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (84, 859442032, 'Rajah', 'Winters', 'm', '2020-07-22', '2117 Tempor St.', 'Little Rock', 'AR', 642720635, 6943681496, 'mollis.lectus.pede@ultrices.ca', 'n', 'n', 'y', 'Lorem ipsum dolor sit');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (85, 625934645, 'Regan', 'Cruz', 'm', '2019-05-30', 'P.O. Box 126, 8992 Praesent Street', 'Butte', 'MT', 408725464, 1326075780, 'est.ac.facilisis@mus.ca', 'n', 'y', 'y', 'Lorem ipsum dolor');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (86, 350178538, 'Erich', 'Joseph', 'm', '2020-05-15', 'Ap #786-4253 Enim Av.', 'Salem', 'OR', 435667742, 4057515268, 'luctus@egestashendrerit.com', 'y', 'y', 'y', 'Lorem');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (87, 252650805, 'Rajah', 'Decker', 'f', '2020-08-05', '8782 Mauris Street', 'Gillette', 'WY', 395418151, 3175008033, 'magna.Duis@lectusconvallisest.ca', 'n', 'n', 'y', 'Lorem ipsum dolor');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (88, 139306829, 'Mollie', 'Gregory', 'f', '2020-06-18', '733-3016 Volutpat. St.', 'Burlington', 'VT', 615930508, 8343812602, 'ut.ipsum@arcuAliquam.co.uk', 'y', 'n', 'n', 'Lorem ipsum');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (89, 461966223, 'Bradley', 'Baird', 'm', '2019-01-11', '476-7002 Nulla Avenue', 'Kailua', 'HI', 893558565, 4853207072, 'erat@auctorodio.com', 'y', 'n', 'y', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (90, 428012799, 'Lucy', 'Jackson', 'm', '2019-04-14', 'P.O. Box 536, 4881 Sapien Street', 'Casper', 'WY', 968861074, 8051674604, 'dapibus@molestie.ca', 'y', 'n', 'n', 'Lorem ipsum');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (91, 871914913, 'Brady', 'Serrano', 'm', '2020-01-03', '870-286 Lacus Rd.', 'San Jose', 'CA', 970049383, 7563320289, 'lorem.tristique@odiotristique.com', 'y', 'n', 'y', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (92, 308436227, 'Alfreda', 'Mcintyre', 'm', '2019-06-06', '9214 Quis Rd.', 'Tacoma', 'WA', 276571850, 1587757882, 'sem.molestie.sodales@semperauctor.org', 'n', 'y', 'y', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (93, 647988120, 'Liberty', 'Bernard', 'm', '2019-11-19', 'Ap #874-2399 Sed Ave', 'Lincoln', 'NE', 791602284, 2290423857, 'egestas.Duis.ac@ipsumnunc.edu', 'n', 'n', 'n', 'Lorem ipsum');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (94, 884714926, 'Holmes', 'Spears', 'f', '2019-07-11', '674-1390 Libero. Ave', 'Davenport', 'IA', 597739766, 6283660980, 'placerat.velit.Quisque@tellusNunclectus.com', 'y', 'n', 'y', 'Lorem');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (95, 759860709, 'MacKenzie', 'Velazquez', 'm', '2019-06-24', 'Ap #701-126 Nascetur Street', 'West Jordan', 'UT', 801894309, 8215888500, 'Donec.est@eu.co.uk', 'n', 'n', 'n', 'Lorem ipsum dolor sit amet,');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (96, 781594979, 'Clarke', 'Woodard', 'f', '2020-04-16', 'P.O. Box 832, 4289 Fermentum Road', 'Frankfort', 'KY', 994162143, 7520405906, 'rhoncus.Proin@pedeet.com', 'y', 'y', 'y', 'Lorem');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (97, 324600125, 'Jenna', 'Reese', 'm', '2020-07-01', '2385 Ligula. St.', 'Frederick', 'MD', 278500995, 9034238019, 'ac.libero.nec@nisi.com', 'y', 'y', 'n', 'Lorem');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (98, 138916655, 'Sybill', 'Brock', 'm', '2018-12-30', '984 Velit. St.', 'Covington', 'KY', 621983200, 6872096461, 'elit.fermentum.risus@tellus.net', 'n', 'y', 'n', 'Lorem ipsum dolor');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (99, 361308711, 'Marcia', 'Mcleod', 'm', '2020-09-21', '3150 Id St.', 'Independence', 'MO', 394935771, 2036797713, 'Nullam.vitae@hendreritDonec.edu', 'y', 'n', 'n', 'Lorem ipsum dolor');
INSERT INTO `acc16`.`person` (`per_id`, `per_ssn`, `per_fname`, `per_lname`, `per_gender`, `per_dob`, `per_street`, `per_city`, `per_state`, `per_zip`, `per_phone`, `per_email`, `per_is_emp`, `per_is_alm`, `per_is_stu`, `per_notes`) VALUES (100, 883746982, 'Piper', 'Bird', 'f', '2019-11-01', '681-8110 Vestibulum Road', 'St. Petersburg', 'FL', 593342751, 2320677793, 'purus.Maecenas@risus.org', 'n', 'y', 'n', 'Lorem ipsum dolor');

COMMIT;


-- -----------------------------------------------------
-- Data for table `acc16`.`employee`
-- -----------------------------------------------------
START TRANSACTION;
USE `acc16`;
INSERT INTO `acc16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (1, 'Registrar', 16, 'n', 'y', 'tempor arcu. Vestibulum ut eros non');
INSERT INTO `acc16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (2, 'Food Service Aide', 23000, 'n', 'y', 'risus. Donec nibh');
INSERT INTO `acc16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (3, 'Program Coordinator', 28000, 'n', 'n', 'Nunc');
INSERT INTO `acc16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (4, 'Financial Aid Specialist', 31000, 'y', 'n', 'tincidunt. Donec vitae erat');
INSERT INTO `acc16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (5, 'Food Service Aide', 34000, 'n', 'y', 'fermentum convallis ligula. Donec luctus aliquet odio. Etiam ligula');
INSERT INTO `acc16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (6, 'Food Service Aide', 36000, 'y', 'n', 'odio. Phasellus at augue id');
INSERT INTO `acc16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (7, 'Program Assistant', 23000, 'n', 'y', 'Quisque tincidunt pede ac urna.');
INSERT INTO `acc16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (8, 'Academic Program Specialist', 34000, 'y', 'y', 'non');
INSERT INTO `acc16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (9, 'School Administrator', 29000, 'y', 'n', 'Donec porttitor tellus non magna. Nam');
INSERT INTO `acc16`.`employee` (`per_id`, `emp_title`, `emp_salary`, `emp_is_fac`, `emp_is_stf`, `emp_notes`) VALUES (10, 'Financial Aid Specialist', 17000, 'n', 'y', 'sagittis lobortis mauris. Suspendisse aliquet');

COMMIT;


-- -----------------------------------------------------
-- Data for table `acc16`.`alumnus`
-- -----------------------------------------------------
START TRANSACTION;
USE `acc16`;
INSERT INTO `acc16`.`alumnus` (`per_id`, `alm_notes`) VALUES (11, 'dictum sapien.');
INSERT INTO `acc16`.`alumnus` (`per_id`, `alm_notes`) VALUES (12, 'justo sit amet nulla. Donec non');
INSERT INTO `acc16`.`alumnus` (`per_id`, `alm_notes`) VALUES (13, 'ipsum sodales purus, in molestie tortor nibh sit');
INSERT INTO `acc16`.`alumnus` (`per_id`, `alm_notes`) VALUES (14, 'eu nulla at sem');
INSERT INTO `acc16`.`alumnus` (`per_id`, `alm_notes`) VALUES (15, 'diam. Duis mi enim, condimentum eget,');
INSERT INTO `acc16`.`alumnus` (`per_id`, `alm_notes`) VALUES (16, 'faucibus');
INSERT INTO `acc16`.`alumnus` (`per_id`, `alm_notes`) VALUES (17, 'Nam ligula elit, pretium et, rutrum non, hendrerit');
INSERT INTO `acc16`.`alumnus` (`per_id`, `alm_notes`) VALUES (18, 'nec,');
INSERT INTO `acc16`.`alumnus` (`per_id`, `alm_notes`) VALUES (19, 'In condimentum. Donec at arcu.');
INSERT INTO `acc16`.`alumnus` (`per_id`, `alm_notes`) VALUES (20, 'suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum ante ipsum');

COMMIT;


-- -----------------------------------------------------
-- Data for table `acc16`.`student`
-- -----------------------------------------------------
START TRANSACTION;
USE `acc16`;
INSERT INTO `acc16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (21, 'Sales and Marketing', 'y', 'n', 'ullamcorper');
INSERT INTO `acc16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (22, 'Research and Development', 'y', 'n', 'feugiat nec, diam. Duis mi enim,');
INSERT INTO `acc16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (23, 'Asset Management', 'y', 'n', 'quis lectus. Nullam suscipit, est ac facilisis facilisis, magna');
INSERT INTO `acc16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (24, 'Public Relations', 'y', 'n', 'pharetra ut, pharetra');
INSERT INTO `acc16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (25, 'Payroll', 'y', 'n', 'id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis');
INSERT INTO `acc16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (26, 'Finances', 'y', 'n', 'imperdiet ornare. In');
INSERT INTO `acc16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (27, 'Human Resources', 'y', 'n', 'tempor lorem, eget mollis lectus pede et risus. Quisque libero');
INSERT INTO `acc16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (28, 'Sales and Marketing', 'y', 'n', 'amet luctus');
INSERT INTO `acc16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (29, 'Quality Assurance', 'y', 'n', 'libero lacus,');
INSERT INTO `acc16`.`student` (`per_id`, `stu_major`, `stu_is_ugd`, `stu_is_grd`, `stu_notes`) VALUES (30, 'Asset Management', 'y', 'n', 'tellus');

COMMIT;


-- -----------------------------------------------------
-- Data for table `acc16`.`faculty`
-- -----------------------------------------------------
START TRANSACTION;
USE `acc16`;
INSERT INTO `acc16`.`faculty` (`per_id`, `fac_rank`, `fac_start_date`, `fac_end_date`, `fac_notes`) VALUES (1, 'Assistant-Professor', '2019-02-19', '2019-09-01', 'vitae velit egestas lacinia. Sed congue, elit sed consequat auctor,');
INSERT INTO `acc16`.`faculty` (`per_id`, `fac_rank`, `fac_start_date`, `fac_end_date`, `fac_notes`) VALUES (2, 'Associate -Professor', '2020-08-08', '2019-05-28', 'interdum. Curabitur dictum. Phasellus in felis. Nulla');
INSERT INTO `acc16`.`faculty` (`per_id`, `fac_rank`, `fac_start_date`, `fac_end_date`, `fac_notes`) VALUES (3, 'Adjunct (pt-time)', '2019-04-04', '2019-09-19', 'diam lorem, auctor quis, tristique ac, eleifend vitae,');
INSERT INTO `acc16`.`faculty` (`per_id`, `fac_rank`, `fac_start_date`, `fac_end_date`, `fac_notes`) VALUES (4, 'Assistant-Professor', '2019-01-31', '2020-04-20', 'accumsan convallis, ante lectus convallis est, vitae');
INSERT INTO `acc16`.`faculty` (`per_id`, `fac_rank`, `fac_start_date`, `fac_end_date`, `fac_notes`) VALUES (5, 'Associate -Professor', '2019-07-10', '2020-09-14', 'ut, molestie in, tempus');

COMMIT;


-- -----------------------------------------------------
-- Data for table `acc16`.`staff`
-- -----------------------------------------------------
START TRANSACTION;
USE `acc16`;
INSERT INTO `acc16`.`staff` (`per_id`, `stf_position`, `stf_start_date`, `stf_end_date`, `stf_notes`) VALUES (6, 'Advertising', '2018-12-28', '2019-11-29 ', 'nunc nulla');
INSERT INTO `acc16`.`staff` (`per_id`, `stf_position`, `stf_start_date`, `stf_end_date`, `stf_notes`) VALUES (7, 'Advertising', '2018-12-11', '2019-05-23', 'sociis');
INSERT INTO `acc16`.`staff` (`per_id`, `stf_position`, `stf_start_date`, `stf_end_date`, `stf_notes`) VALUES (8, 'Tech Support', '2019-07-16 ', '2019-10-13 ', 'Donec elementum, lorem ut aliquam iaculis,');
INSERT INTO `acc16`.`staff` (`per_id`, `stf_position`, `stf_start_date`, `stf_end_date`, `stf_notes`) VALUES (9, 'Payroll', '2020-07-18 ', '2020-07-30', 'Duis cursus, diam at pretium aliquet,');
INSERT INTO `acc16`.`staff` (`per_id`, `stf_position`, `stf_start_date`, `stf_end_date`, `stf_notes`) VALUES (10, 'Customer Service', '2018-06-10 ', '2019-11-23', 'malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in');

COMMIT;


-- -----------------------------------------------------
-- Data for table `acc16`.`degree`
-- -----------------------------------------------------
START TRANSACTION;
USE `acc16`;
INSERT INTO `acc16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (11, 11, 'Associate', 'Quality Assurance', '2020-08-27', 'sodales purus, in molestie tortor nibh sit amet orci. Ut');
INSERT INTO `acc16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (12, 12, 'Doctorate', 'Quality Assurance', '2018-11-07', 'varius et, euismod et, commodo at, libero. Morbi accumsan laoreet');
INSERT INTO `acc16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (13, 13, 'Master\'s', 'Finances', '2020-01-28', 'sapien. Aenean massa. Integer vitae nibh.');
INSERT INTO `acc16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (14, 14, 'Master\'s', 'Tech Support', '2020-08-24', 'turpis. Aliquam adipiscing');
INSERT INTO `acc16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (15, 15, 'Bachelor\'s', 'Quality Assurance', '2019-05-06', 'ornare. Fusce');
INSERT INTO `acc16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (16, 16, 'Doctorate', 'Payroll', '2019-01-01', 'enim commodo hendrerit. Donec porttitor tellus');
INSERT INTO `acc16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (17, 17, 'Master\'s', 'Finances', '2019-02-01', 'parturient');
INSERT INTO `acc16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (18, 18, 'Associate', 'Tech Support', '2019-05-27', 'aliquet vel, vulputate eu, odio. Phasellus');
INSERT INTO `acc16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (19, 19, 'Bachelor\'s', 'Asset Management', '2019-05-31', 'enim, gravida sit amet, dapibus');
INSERT INTO `acc16`.`degree` (`deg_id`, `per_id`, `deg_type`, `deg_area`, `deg_date`, `deg_notes`) VALUES (20, 20, 'Bachelor\'s', 'Customer Service', '2020-07-16', 'purus sapien,');

COMMIT;


-- -----------------------------------------------------
-- Data for table `acc16`.`undergrad`
-- -----------------------------------------------------
START TRANSACTION;
USE `acc16`;
INSERT INTO `acc16`.`undergrad` (`per_id`, `ugd_test`, `ugd_score`, `ugd_standing`, `ugd_start_date`, `ugd_end_date`, `ugd_notes`) VALUES (21, 'act', 29, 'senior', '2019-06-07 ', '2019-11-27 ', 'lacinia mattis. Integer eu lacus. Quisque imperdiet,');
INSERT INTO `acc16`.`undergrad` (`per_id`, `ugd_test`, `ugd_score`, `ugd_standing`, `ugd_start_date`, `ugd_end_date`, `ugd_notes`) VALUES (22, 'sat', 14, 'senior', '2019-07-07', '2018-12-08', 'eu lacus. Quisque imperdiet, erat');
INSERT INTO `acc16`.`undergrad` (`per_id`, `ugd_test`, `ugd_score`, `ugd_standing`, `ugd_start_date`, `ugd_end_date`, `ugd_notes`) VALUES (23, 'sat', 27, 'sophomore', '2020-07-20', '2018-12-16', 'erat volutpat. Nulla dignissim. Maecenas ornare');
INSERT INTO `acc16`.`undergrad` (`per_id`, `ugd_test`, `ugd_score`, `ugd_standing`, `ugd_start_date`, `ugd_end_date`, `ugd_notes`) VALUES (24, 'sat', 26, 'freshman', '2019-03-13 ', '2019-06-25', 'ultricies ligula. Nullam enim. Sed nulla ante, iaculis');
INSERT INTO `acc16`.`undergrad` (`per_id`, `ugd_test`, `ugd_score`, `ugd_standing`, `ugd_start_date`, `ugd_end_date`, `ugd_notes`) VALUES (25, 'sat', 19, 'freshman', '2020-04-08 ', '2018-11-25', 'Integer vitae nibh. Donec');

COMMIT;


-- -----------------------------------------------------
-- Data for table `acc16`.`grad`
-- -----------------------------------------------------
START TRANSACTION;
USE `acc16`;
INSERT INTO `acc16`.`grad` (`per_id`, `grd_test`, `grd_score`, `grd_start_date`, `grd_end_date`, `grd_notes`) VALUES (26, 'gmat', 15, '2020-01-25 ', '2020-03-30 ', 'quam. Curabitur vel lectus. Cum sociis');
INSERT INTO `acc16`.`grad` (`per_id`, `grd_test`, `grd_score`, `grd_start_date`, `grd_end_date`, `grd_notes`) VALUES (27, 'lsat', 29, '2020-02-01', '2019-05-25', 'Nullam nisl. Maecenas malesuada fringilla est. Mauris eu');
INSERT INTO `acc16`.`grad` (`per_id`, `grd_test`, `grd_score`, `grd_start_date`, `grd_end_date`, `grd_notes`) VALUES (28, 'sat', 28, '2019-02-16', '2019-04-30', 'sed, sapien. Nunc pulvinar arcu et pede. Nunc sed');
INSERT INTO `acc16`.`grad` (`per_id`, `grd_test`, `grd_score`, `grd_start_date`, `grd_end_date`, `grd_notes`) VALUES (29, 'act', 19, '2019-09-14', '2019-05-06', 'Curabitur dictum. Phasellus in felis. Nulla tempor augue ac ipsum.');
INSERT INTO `acc16`.`grad` (`per_id`, `grd_test`, `grd_score`, `grd_start_date`, `grd_end_date`, `grd_notes`) VALUES (30, 'sat', 14, '2020-03-13', '2020-09-05', 'id enim. Curabitur massa. Vestibulum');

COMMIT;

